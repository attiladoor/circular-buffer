#include <limits.h>
#include <memory>

#include "gtest/gtest.h"
#include "circular_buffer.h"

using namespace std;

class CircularBufferTest: public ::testing::Test {
protected:
    void SetUp() { };

    void TearDown() { }
};

TEST_F(CircularBufferTest, empty1)
{
    TCircularBuffer<int> bb;
    EXPECT_EQ(bb.Empty(), true);
}

TEST_F(CircularBufferTest, emtpy2)
{
    TCircularBuffer<int> bb;
    bb.Push(1);
    EXPECT_EQ(bb.Empty(), false);
}

TEST_F(CircularBufferTest, Size1)
{
    TCircularBuffer<int> bb;
    for(int i = 0; i < 22; i++) {
       bb.Push(i);
    }
   EXPECT_EQ(bb.CurrentSize(), BUFFER_SIZE);
}

TEST_F(CircularBufferTest, Size2)
{
    TCircularBuffer<int> bb;
    for(int i = 0; i < 8; i++) {
       bb.Push(i);
    }
   EXPECT_EQ(bb.CurrentSize(), 8);
}

TEST_F(CircularBufferTest, Size3)
{
    TCircularBuffer<int> bb;
    EXPECT_EQ(bb.CurrentSize(), 0);
}


TEST_F(CircularBufferTest, Size4)
{
    TCircularBuffer<int> bb;
    for (int i = 0; i < 40; i++) {
        bb.Push(i);
    }
    bb.Pop();
    bb.Pop();
    EXPECT_EQ(bb.CurrentSize(), BUFFER_SIZE - 2);
}

TEST_F(CircularBufferTest, Pop1)
{
    TCircularBuffer<int> bb;
    int a;
    bb.Push(1);
    bb.Push(2);
    bb.Peek(a);
    EXPECT_EQ(a, 1);
}

TEST_F(CircularBufferTest, Pop2)
{
    TCircularBuffer<int> bb;
    int a;
    bb.Push(1);
    bb.Push(2);
    bb.Pop();
    bb.Peek(a);
    EXPECT_EQ(a, 2);
}

TEST_F(CircularBufferTest, PushPop)
{
    TCircularBuffer<int> bb;
    int a;
    for (int i = 0; i < 40; i++) {
        bb.Push(i);
    }
    bb.Peek(a);
    EXPECT_EQ(a, 40-BUFFER_SIZE);
}

TEST_F(CircularBufferTest, increment1) {
    int a = 1;
    TCircularBuffer<int>::circular_increment(a, 10);
    EXPECT_EQ(a, 2);
}

TEST_F(CircularBufferTest, increment2) {
    int a = 9;
    TCircularBuffer<int>::circular_increment(a, 10);
    EXPECT_EQ(a, 0);
}


int main(int argc, char **argv) {
  ::testing::InitGoogleTest(&argc, argv);
  return RUN_ALL_TESTS();
}
