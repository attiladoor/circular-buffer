# Circular buffer

### Assignment:
Implement a circular (ring) buffer that may store a user defined number of uint32_t types. This buffer should be a First-in First-out implementation.
You may use C or C++, and are encouraged to use the language that is most comfortable for you. Your implementation should be appropriate for
a lightweight embedded system.
Your Circular Buffer should implement the following operations:
- Push
- Pop
- Peek
- Current Size
- Maximum Size
- Empty
- Full

### Deliverables:
- Source file(s) implementing the circular buffer functionality
- Source file(s) exercising the circular buffer functionality
- This can be implemented using a unit test framework, but this is not required
- You may define the inputs for the exercising code
- Instructions to obtain and build the source, as well as run the executable

### Limitations
- Existing circular buffer libraries should not be used
- Heap allocations and exceptions should not be used
- Tools used should be publicly available (free/open source), and should work with modern macOS and/or Linux distributions (Debian/Ubuntu preferred)

### Allowances
- It is not necessary to guarantee thread safety (assume host environment assures this)
- You may define either the circular buffer size or the maximum circular buffer size at compile time
- Your code should be aimed at a lightweight embedded system, however your code will be built/run on a PC

## Build and run:
Build and run the example with:
```bash
make
./circular_buffer_example

```

or build and run the unit tests with:
```bash 
make test
./circular_buffer_test
```

Running unit tests requires GTest unit testing framework, read more: https://gitlab.com/attiladoor/GoogleTestTutorial

