#ifndef CIRCULAR_BUFFER_H
#define CIRCULAR_BUFFER_H

#define BUFFER_SIZE     10

template <typename T> class TCircularBuffer
{
private:
    T buffer[BUFFER_SIZE];
    int i_first = 0, i_last = -1;
    bool EmptyFlag = true;

public:
    TCircularBuffer() {};
    ~TCircularBuffer() {};

    /*  @brief  add new element to circular buffer
     *  @retval none, it must be always successful
     */

    void Push(const T new_element) {
        int prev_i_last = i_last;
        circular_increment(i_last, BUFFER_SIZE);
        buffer[i_last] = new_element;
        if ( (prev_i_last != -1) && (i_last == i_first)) {
            circular_increment(i_first, BUFFER_SIZE);
        }
        EmptyFlag = false;
    }

    /*  @brief remove the oldest member of the buffer
     *  @retval 1: if buffer is emtpy
     *          0: if success
     */

    int Pop() {
        if(EmptyFlag) {
            return 1;
        }

        if (CurrentSize() == 1) {
            EmptyFlag = true;
        }
        
        circular_increment(i_first, BUFFER_SIZE);
        return 0;
    }

    /*  @brief  peeking the oldest member of the buffer over a reference
     *  @retval 1: if buffer is empty
     *          0: if success
     */ 

    int Peek(T& retval) {
        if(EmptyFlag) {
            return 1;
        }
        
        retval = buffer[i_first];
        return 0;
    }

    /*  @brief  get size of buffer
     *  @retval nummber of elements in buffer as integer
     */

    int CurrentSize() {

        if(EmptyFlag) {
            return 0;
        }

        int diff = i_last - i_first + 1;
        if ( diff > 0) {
            return diff; 
        } else {
            return BUFFER_SIZE + diff;
        }
    }

    /*  @brief  get aximum number of elements
     *  @retval max element number as integer
     */

    int MaximumSize() {
        return BUFFER_SIZE;
    }
    
    /*  @brief  get if buffer is empty
     *  @retval boolean value
     */
    bool Empty() {
        return (CurrentSize() == 0);       
    }

    /*  @brief  get if buffer is full 
     *  @retval boolean value
     */

    bool Full() {
        return (CurrentSize() == BUFFER_SIZE);
    }

    /*  @brief  helper function 
    */

    static void circular_increment(int & a, int maximum) 
    {
        a++;
        if (a == maximum) {
            a = 0;
        }
    }
};

#endif