CXX=g++ -std=c++14
CXXFLAGS_TEST = -g -lgtest -lpthread


BIN=circular_buffer_example
TEST_BIN=circular_buffer_test

$(BIN): main.o
	$(CXX) $^ -o $@

$(TEST_BIN): circular_buffer_test.o 
	$(CXX)  $^ -o $@ $(CXXFLAGS_TEST)
 
all : $(BIN)

test: $(TEST_BIN)

clean:
	rm -f *.o $(BIN) $(TEST_BIN)