#include <iostream>
#include <stdint.h>
#include "circular_buffer.h"

using namespace std;

int main()
{        
    uint32_t a;
    TCircularBuffer<uint32_t> b;

    cout << "\nADD ELEMENTS\n" << endl;
    for (int i = 0; i < 20;  i++) {
        b.Push(i);
        b.Peek(a);
        cout << "Empty: " << b.Empty() << " \tFull: " << b.Full() << " \tSize: " << b.CurrentSize() <<  " \tElement: " << a << endl;
    }
    cout << "\nREMOVE ELEMENTS\n" << endl;

    for (int i = 0; i < 12;  i++) {

        cout << "Empty: " << b.Empty() << " \tFull: " << b.Full() << " \tSize: " << b.CurrentSize() <<  " \tElement: " << a << endl;

        if (b.Pop()) {
            cout << "error: empty buffer" << endl;
            continue;
        }       
        if (b.Peek(a)) {
            cout << "error: empty buffer" << endl;
            continue;
        }
    }

}